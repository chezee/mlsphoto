//
//  ViewController.h
//  MLSPhoto
//
//  Created by Илья Пупкин on 1/19/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKSdk.h"


@interface MLSLoginViewController : UIViewController <VKSdkDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableViewAlbums;
@property (weak, nonatomic) NSString *accessToken;

@end

