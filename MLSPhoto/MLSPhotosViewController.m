//
//  MLSPhotosViewController.m
//  MLSPhoto
//
//  Created by Илья Пупкин on 1/19/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MLSPhotosViewController.h"
#import "MLSPhotoViewController.h"

static NSString *cellIdentifier = @"cellIdentifier";

@interface MLSPhotosViewController ()

@end

@implementation MLSPhotosViewController {
    UITableView *photosTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [UITableViewCell new];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                           forIndexPath:indexPath];
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = [photosTable indexPathForSelectedRow];
    
    MLSPhotoViewController *photoDetail = segue.destinationViewController;
}

@end
