//
//  MLSAlbumController.m
//  MLSPhoto
//
//  Created by Илья Пупкин on 1/19/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MLSAlbumController.h"
#import "MLSPhotosViewController.h"

static NSString *cellIdentifier = @"cellIdentifier";

@interface MLSAlbumController ()

@end

@implementation MLSAlbumController {
    UITableView *albumsTable;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [UITableViewCell new];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                           forIndexPath:indexPath];
    return cell;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = [albumsTable indexPathForSelectedRow];
    
    MLSPhotosViewController *photosList = segue.destinationViewController;
}


@end
