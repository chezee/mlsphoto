//
//  ViewController.m
//  MLSPhoto
//
//  Created by Илья Пупкин on 1/19/17.
//  Copyright © 2017 Ilya Gorevoy. All rights reserved.
//

#import "MLSLoginViewController.h"
#import "MLSAlbumController.h"

@interface MLSLoginViewController ()

@end

@implementation MLSLoginViewController {
    UITextField *username;
    UITextField *password;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[VKSdk initializeWithAppId:@"5713645"] registerDelegate:self];
    NSArray *SCOPE = @[@"friends", @"email", @"photos"];
    
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        NSLog(@"state is:%d", state);
        if (state == VKAuthorizationAuthorized) {
            self.view.hidden = YES;
            self.tableViewAlbums.hidden = NO;
        } else if (error) {
            NSLog(@"%@", error);
        } else {
            self.view.hidden = NO;
            self.tableViewAlbums.hidden = YES;
        }
    }];
    [self.view setBackgroundColor:[UIColor grayColor]];
    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x - 30,
                                                                       self.view.center.y + 30,
                                                                       60,
                                                                       20)];
    [loginButton addTarget:self action:@selector(loginTo) forControlEvents:UIControlEventTouchDown];
    [loginButton setTitle:@"Log in" forState:UIControlStateNormal];
    [self.view addSubview:loginButton];
    
    username = [[UITextField alloc] initWithFrame:CGRectMake(self.view.center.x - 40,
                                                             self.view.center.y - 30,
                                                             120,
                                                             20)];
    username.backgroundColor = [UIColor whiteColor];
    username.placeholder = @"login";
    [self.view addSubview:username];
    
    password = [[UITextField alloc] initWithFrame:CGRectMake(self.view.center.x - 40,
                                                             self.view.center.y,
                                                             120,
                                                             20)];
    password.backgroundColor = [UIColor whiteColor];
    password.placeholder = @"password";
    [self.view addSubview:password];
    
}

- (void) vkSdkUserAuthorizationFailed {
    
}

- (void) vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    self.accessToken = result.token.accessToken;
    NSLog(@"%@", result.token.accessToken);
    [self.navigationController presentViewController:[MLSAlbumController new]
                                            animated:YES
                                          completion:nil];
    
}

- (void) loginTo {
    NSArray *scope = @[@"friends", @"email", @"photos"];
    [VKSdk authorize:scope];
    NSLog(@"%@", self.accessToken);
}


@end
